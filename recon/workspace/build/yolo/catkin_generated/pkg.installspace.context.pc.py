# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lyolo".split(';') if "-lyolo" != "" else []
PROJECT_NAME = "yolo"
PROJECT_SPACE_DIR = "/home/ubuntu/Documents/amr/recon/workspace/install"
PROJECT_VERSION = "0.0.0"
